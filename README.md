#### Demonstrasi A* Algorithm

Program ini dibuat dengan Processing untuk mendemonstrasikan (dan sekaligus mengvisualisasikan) A* algorithm.

Feel free to use this program as you see fit. There's no need to contact me.

#### Author

Suhendi,
suhendi999@gmail.com,
Mahasiswa Informatika ITB.
